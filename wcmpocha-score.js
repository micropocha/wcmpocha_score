(async () => {

  class WCMPochaScore extends HTMLElement {
    constructor() {  
      super();
      this._initializated = false;
    }

    _initialize(){
      if (this._initializated == false){
        this._initializated = true;
        // define properties
        this._createProp("player");
        this._createProp("score");
        this._createProp("round");
        this._createProp("delta");
        this._createProp("button_plus");
        this._createProp("button_minus");
        // defaultValues
        if(this.button_plus === undefined) this.button_plus = "+";
        if(this.button_minus === undefined) this.button_minus = "-";
        // create shadowdoom
        this._root = this.attachShadow({mode: 'closed'});
        this._template = WCMPochaScore.createTemplate();
        this.render();
        this._addEventListeners();
      }
    }

    static get observedAttributes() {
      return ['player', 'score', 'round', 'delta', 'button_plus', 'button_minus']; 
    };

    static createTemplate(){
      function createStyle(){
        return `
        :host {font-family: var (--fontFamily, "Lato");}
        button { 
          background-color: var(--scoreButtonsBackgroundColor, cornflowerblue); 
          border-radius: 10px; font-family: "Lato"; font-weight: 300;}
        `
      };
      function createBody(){
        return ` 
        <style>${createStyle()}</style>
          <button id="minusButton"> 
            <slot name="button_minus"></slot>
          </button>
          <span>Player; <slot name="player"></slot>: 
          has <slot name="score"></slot> points
          <button id="plusButton"> 
            <slot name="button_plus"></slot>
          </button>
        </span>`;
      }
      var wrapper = document.createElement("template")
      wrapper.innerHTML = createBody();
      return wrapper;
    }

    render(){
      this._root.appendChild ( this._template.content.cloneNode(true) );
    }

    _createProp(name , reflect=true, slotable=1){
      self = this;
      const times = n => Array.from(Array(n).keys()) 

      function createSlottedTag (slotIndex) {
        const indexName = (index) => {return index?index:''}   
        var htmlTagAttribute = document.createElement ( "span" );
        htmlTagAttribute.slot = name + indexName(slotIndex) 
        self.appendChild(htmlTagAttribute);
        return htmlTagAttribute;
      }

      function createPropertyStructure (name, reflect, slotable){
        if (self._autoProperties===undefined) self._autoProperties = {};
        self._autoProperties[name]={
          value: undefined,
          slotable: times(slotable).map( createSlottedTag ),
          reflect: reflect
        }
      }

      function createPropertySetterAndGetter(thisObject){
        Object.defineProperty(thisObject, name, {
          set: (value) => {
            var prop = thisObject._autoProperties[name];
            prop.value = value;
            prop.slotable.map(x => x.textContent = value);
            if (prop.reflect) 
              thisObject.setAttribute(name, value); 
          },
          get: () => {
            return thisObject._autoProperties[name].value;
          }
        });
      }
      
      createPropertyStructure (name, reflect, slotable);
      createPropertySetterAndGetter(this);
    }

    // Fires when an attribute was added, removed, or updated (only for listened in observedAttributes)
    attributeChangedCallback (attr, oldVal, newVal) {
      if (oldVal == newVal) return;
      console.log(`${attr} was changed from ${oldVal} to ${newVal}!`);
      switch (attr) {
        case "player":
        case "button_plus":
        case "button_minus":
        case "score":
        case "round":
        case "delta":
          this[attr]=newVal;
          break;
        default:
          break;
      }
    };

    _addEventListeners (){
      var me = this;
      function raiseCustomEventOnClick (elementId, nameEvent) {
        var root = me._root;
        var element = root.getElementById(elementId);
        element.addEventListener("click", (e,f) => {
          if(e.isTrusted){
            var parameters = {player: me.player};
            me.dispatchEvent(new CustomEvent(nameEvent, {detail: parameters, bubbles: true}))
          }
        });
      }
      raiseCustomEventOnClick('minusButton', 'minus');
      raiseCustomEventOnClick('plusButton', 'plus');
    }

    // Fires when an instance of the element is created
    createdCallback () {};
    // Fires when an instance was inserted into the document
    attachedCallback () {};
    // Fires when an instance was removed from the document
    detachedCallback () {};
    // Called when element is inserted in DOM
    connectedCallback() {
      this._initialize();
    };
    
  }
  
  customElements.define('wcmpocha-score', WCMPochaScore);
})();