Description
==================================

this component shows and handles the score of a player

## attributes (html):

- player
- score
- round
- button_plus
- button_minus

## properties (js)

- player
- score
- round

## events

- plus (player)
- minus (player)

## listens


## style
- --scoreButtonsBackgroundColor
- --fontFamily
- winner
- loser

Run
=======================================

## run first time

```bash
npm install 
npm start
```

## test

```bash
npm test
```

ref - https://meowni.ca/posts/styling-the-dome/